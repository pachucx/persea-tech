import time
import urllib2

import cv2
import numpy as np
import time
import logging
import os
# Get an instance of a logger
LOG = logging.getLogger(__name__)

from django.conf import settings

# import threading
# from threading import Thread, Event, ThreadError

class Kupa(object):

    def __init__(self, kupa):
        self.kupa = kupa

class faceDetector(object):
    def __init__(self, camera_id):
        self.frame_number = 0
        self.camera_id = camera_id
        self.CASC_PATH = os.path.join(settings.BASE_DIR, 'haarcascade_frontalface_default.xml')
        LOG.info(self.CASC_PATH)
        self.img = np.zeros((480, 640, 3), dtype=np.uint8)
        self.faceCascade = cv2.CascadeClassifier(self.CASC_PATH)
        LOG.info(self.camera_id)

    def load_img(self, img, frame_number):
        self.img = img
        self.frame_number = frame_number
        return self

    def detect(self):
        # Read the image
        gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)

        # Detect faces in the image
        faces = self.faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags = cv2.CASCADE_SCALE_IMAGE
        )

        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(self.img, (x, y), (x+w, y+h), (0, 255, 0), 2)
            self.save_face(x, y, w, h)
        return self.img

    def save_face(self, x, y, w, h):
        face = self.img[y:(y+h), x:(x+w)]
        static_dir = os.path.join(settings.BASE_DIR, 'static')
        cameras_dir = os.path.join(static_dir, 'cameras')
        camera_dir = os.path.join(cameras_dir, 'cam_{0}'.format(self.camera_id))
        file_name = os.path.join(camera_dir, 'frame_{0}.png'.format(self.frame_number))
        LOG.info('writing to {0} shape {1}'.format(file_name, face.shape))
        cv2.imwrite(file_name, face)

class IpCamera(object):

    def __init__(self, url, camera_id):
        self.url = url
        self.camera_id = camera_id
        from .models import Camera
        self.db_hook = Camera.objects.get(id=camera_id)
        LOG.info(self.camera_id)

        LOG.info("url {0}".format(url))

        # Camera specific
        self.stream = urllib2.urlopen(url)

        # stream settings
        self.buffer_size = 1024
        self.bytes = ''

        # frame capturing params
        self.frame_counter = 0
        self.frame = np.zeros((480, 640, 3), dtype=np.uint8)
        self.init_time = time.time()

        # frame computation 
        # self.thread = Thread(target=self._run)
        # self.thread_cancelled = False
        LOG.info('Camera {0} initialised'.format(url))

        self.face_detector = faceDetector(self.camera_id)

    def _compute_frame(self):
    	LOG.info('ID {0} computing frame'.format(self.camera_id))
        # Load bytes from stream
        t_start = time.time()
        self.bytes += self.stream.read(1024*4)
        t_end = time.time()
        # print 'Bytes loading: {0:.5f}'.format(t_end - t_start)

        t_start = time.time()
        # Find begin byte
        a = self.bytes.find('\xff\xd8')

        # Find end byte
        b = self.bytes.find('\xff\xd9')
        t_end = time.time()

        # print 'Localising frame bytes: {0:.5f}'.format(t_end - t_start)
        if a!=-1 and b!=-1:
            # Look for content between a and b bytes
            jpg = self.bytes[a:b+2]

            # Concatenate extracted content
            self.bytes = self.bytes[b+2:]

            # Decode frame
            t_start = time.time()
            self.frame = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
            t_end = time.time()
            # print 'Decoding frame: {0:.5f}'.format(t_end - t_start)
            
            self._increment()
            # print self.frame_counter
            LOG.info('ID {0} Frame counter: {1}'.format(self.camera_id, self.frame_counter))

            # name = PATH + '/cameras/faces/camera__frame_{0}.png'.format(self.frame_counter)
            # LOG.info('writing to {0} shape {1}'.format(name, self.frame.shape))
            # cv2.imwrite(name, self.frame)

            self.frame = self.face_detector.load_img(self.frame, self.frame_counter).detect()

    def get_frame(self):
        self._compute_frame()
        # print len(self.bytes)
        return self.frame

    def get_fps(self):
        if self.frame_counter:
            LOG.info("{0:.2f} fps".format(self.frame_counter / (time.time() - self.init_time)))
        pass

    def get_frame_counter(self):
        return self.frame_counter

    # Thread specific methods

    # def _run(self):
    #     while not self.thread_cancelled:
    #         try:
    #             self._compute_frame()
    #         except ThreadError:
    #             LOG.info('ThreadError at {0}'.format(self.id))
    #             self.thread_cancelled = True

    # def start(self):
    #     self.thread.start()
    #     LOG.info('camera {0} stream started'.format(self.id))

    # def is_running(self):
    #     LOG.info('camera {0} is running {1}'.format(self.id, self.thread.isAlive()))
          
    # def shut_down(self):
    #     LOG.info('stoping camera {0}'.format(self.id))
    #     self.thread_cancelled = True
    #     # block while waiting for thread to terminate
    #     while self.thread.isAlive():
    #       time.sleep(1)
    #     self.is_running()
    #     return True

    def _increment(self):
        self.frame_counter += 1
        if self.frame_counter % 10 == 0:
            LOG.info('ID {0} updating frame counter to {1}'.format(self.camera_id, self.frame_counter))
            self._update_hook()

    def _update_hook(self):
        # update hook
        from .models import Camera
        self.db_hook = Camera.objects.get(id=self.camera_id)
        self.db_hook.frame_counter = self.frame_counter
        self.db_hook.fps = "{0:.2f} fps".format(self.frame_counter / (time.time() - self.init_time))
        self.db_hook.save()

    def get_counter(self):
        LOG.info('counter {0}'.format(self.frame_counter))
