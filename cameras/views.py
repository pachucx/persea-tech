from django.shortcuts import render, redirect

from models import *
from persea.celery import app
from django.conf import settings

import pickle
import logging
LOG = logging.getLogger(__name__)
import os

import glob

PATH = os.getcwd()

def index(request):
    LOG.info('index')
    cameras = Camera.objects.all()
    # absolute_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'cameras.obj')

    # LOG.info(os.path.exists(absolute_path))

    # ip_cameras = pickle.load(open(absolute_path, 'rb'))

    return render(request, 'cameras/cameras.html', {'cameras' : cameras})

def add(request):
    LOG.info('add request')
    if request.POST:
        name = request.POST.get('name', None)
        url = request.POST.get('url', None)
        if name and url:
            LOG.info('{0} {1}'.format(name, url))
            camera = Camera(name=name, url=url, frame_counter=0, fps='0 fps').save()
            LOG.info(camera)
            camera.start()
            LOG.info('saved')
    return redirect('/cameras')

def faces(request):
    LOG.info('shows faces')
    camera_id = request.GET.get('camera_id', None)
    static_dir = os.path.join(settings.BASE_DIR, 'static')
    cameras_dir = os.path.join(static_dir, 'cameras')
    camera_dir = os.path.join(cameras_dir, 'cam_{0}'.format(camera_id))
    file_name_query = os.path.join(camera_dir, 'frame_*')

    LOG.info(file_name_query)
    found_paths = glob.glob(file_name_query)
    LOG.info(found_paths)
    paths = []
    for path in found_paths:
        paths.append(path.replace(settings.BASE_DIR, ''))
    context = {'paths' : paths, 'camera': Camera.objects.get(id=camera_id)}
    return render(request, 'cameras/faces.html', context)

def clean(request):
    LOG.info('clean')

    cameras_dict = {}
    for camera in Camera.objects.all():
        # kill
        camera.status = DEACTIVE
        camera.save()

        # Collect duplicates in database
        if not camera.url in cameras_dict:
            cameras_dict[camera.url] = []
        cameras_dict[camera.url].append(camera.id)

    # Remove duplicate urls in database 
    for url, list_of_ids in cameras_dict.iteritems():
        if len(list_of_ids) > 1:
            for camera_id in list_of_ids[1:]:
                LOG.info('Camera with id {0} is scheduled for removal'.format(camera_id))
                Camera.objects.get(id=camera_id).delete()

    return redirect('/cameras')

def activate(request):  
    # initialise all tasks
    for camera in Camera.objects.all():
        camera.activate()
    return redirect('/cameras')

def schedule_for_deactivation(request):
    # Deactivate all tasks
    for camera in Camera.objects.all():
        camera.schedule_for_deactivation()
    return redirect('/cameras') 

def kill(request): 
    for camera in Camera.objects.all():
        camera.kill()    
    return redirect('/cameras')
