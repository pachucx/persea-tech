from django.conf.urls import url

from . import views

app_name = 'cameras'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add/$', views.add, name='add'),
    url(r'^faces/$', views.faces, name='faces'),    
    url(r'^clean/$', views.clean, name='clean'),  
    url(r'^activate/$', views.activate, name='activate'),     
    url(r'^schedule_for_deactivation/$', views.schedule_for_deactivation, name='schedule_for_deactivation'),     
    url(r'^kill/$', views.kill, name='kill'),     
]