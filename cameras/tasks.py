from celery.decorators import task

from ip_camera import IpCamera, Kupa
# from models import *

ACTIVE = 0
SCHEDULED_FOR_DEACTIVATION = 1
DEACTIVE = 2

import logging
LOG = logging.getLogger(__name__)

@task(name="start_streaming_task")
def start_streaming_task(camera_id, name, url):
    """starts ipCamera streaming"""
    LOG.info("start_streaming called")

    LOG.info("id {0} name {1} url {2}".format(camera_id, name, url))

    ip_cam = IpCamera(url, camera_id)

    # ip_cam = ipCamera(url)
    # camera = Camera.objects.get(id=camera_id)
    while True:
        # LOG.info("kupa")
        ip_cam.get_frame()
        ip_cam.get_fps()

        if ip_cam.get_frame_counter() % 10 == 0:
            LOG.info('ID {0} active is {1}'.format(ip_cam.camera_id, ip_cam.db_hook.status))
            if ip_cam.db_hook.status == SCHEDULED_FOR_DEACTIVATION or \
                                     ip_cam.db_hook.status == DEACTIVE:
                # Set status as dead
                ip_cam.db_hook.status = DEACTIVE
                ip_cam.db_hook.fps = "0 fps"
                ip_cam.db_hook.save()
                break

    LOG.info("id {0} name {1} url {2} EXITED".format(camera_id, name, url))        
        # if ip_cam.get_frame_counter() % 10:
        #     camera.frame_counter = p_cam.get_frame_counter()
        #     camera.save()




