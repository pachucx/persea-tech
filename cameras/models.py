from __future__ import unicode_literals

from django.db import models
from django.conf import settings

from django.db.models.signals import post_save
from django.dispatch import receiver

from .tasks import *
from persea.celery import app
from ip_cameras import ipCamera

import logging
import os

# Get an instance of a logger
LOG = logging.getLogger(__name__)

ACTIVE = 0
SCHEDULED_FOR_DEACTIVATION = 1
DEACTIVE = 2

class Camera(models.Model):
    name = models.CharField(max_length=255, default='camera')
    url = models.CharField(max_length=255)
    init_time = models.DateTimeField('initialised', auto_now_add=True)
    frame_counter = models.IntegerField(default=0)
    task_id = models.CharField(max_length=255)
    status = models.IntegerField(default=DEACTIVE)
    fps = models.CharField(max_length=10, default='0 fps')

    def __str__(self):
        return '{0} {1}'.format(self.name, self.url)

    def activate(self, *args, **kwargs):
        LOG.info('initialising camera {0}'.format(self))

        # create face directory in /static/cameras
        static_dir = os.path.join(settings.BASE_DIR, 'static')
        cameras_dir = os.path.join(static_dir, 'cameras')
        camera_dir = os.path.join(cameras_dir, 'cam_{0}'.format(self.id))
        if not os.path.exists(camera_dir):
            os.makedirs(camera_dir)

        # start task        
        res = start_streaming_task.delay(self.id, self.name, self.url)
        self.task_id = res.id
        self.status = ACTIVE
        self.save()

    def schedule_for_deactivation(self, *args, **kwargs):
        LOG.info('stopping camera {0}'.format(self))
        self.status = SCHEDULED_FOR_DEACTIVATION
        self.save()

    def kill(self, *args, **kwargs):
        LOG.info('killing camera {0}'.format(self))
        self.status = DEACTIVE
        self.save()
        # kill all running tasks
        LOG.info('killing {0}'.format(self.task_id))
        r = app.control.revoke(str(self.task_id), terminate=True)#, signal='SIGKILL')
        LOG.info('killed {0}'.format(r))

# @receiver(post_save, sender=Camera)
# def initialise_ip_camera(sender, instance, **kwargs):
#     LOG.info('initialise_ip_camera signal received')
#     LOG.info('{0}'.format(instance))

#     # start task
#     res = start_streaming_task.delay(instance.url, instance.name)
#     intance.task_id = res.id
#     instance.save()


    # ip_cameras.append(camera)

    # pickle.dump(ip_cameras, open(absolute_path, 'wb'))
 